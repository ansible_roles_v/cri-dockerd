Kubernetes
=========
Installs CRI-Dockerd as Docker Engine shim for Kubernetes using ready binary.  

Requirements:
-------------
Docker Engine installed.

Include role
------------
```yaml
- name: cri_dockerd  
  src: https://gitlab.com/ansible_roles_v/cri-dockerd/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - cri_dockerd
```